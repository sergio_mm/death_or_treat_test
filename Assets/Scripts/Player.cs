using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
   // public float jumpforce = 400;
    public float maxSpeed = 4;
    private float currentSpeed;
    //Jumping
    public float jumpH;
    public float jumpforce;
    private Vector3 jump;
    private Rigidbody rigg;

    private Rigidbody rb;
    private Animator anim;
    private Transform groundCheck;
    private bool onGround;
    private bool isDead = false;
    private bool facingRight = true;
    
    //private bool jump = false;
  
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        groundCheck = gameObject.transform.Find("GroundCheck");
        currentSpeed = maxSpeed;
        jump = new Vector3(0f, jumpH, 0f);
        rigg = GetComponent<Rigidbody>();

        
    }

   
    void Update()
    {
        onGround = Physics.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        /* if (Input.GetButtonDown("Jump") && onGround)
         {
             jump = true;
         }*/
        if (rigg.velocity.y == 0 ) {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                rigg.AddForce(jump * jumpforce, ForceMode.Impulse);
            }
        }

    }
    private void FixedUpdate()
    {
        if (!isDead)
        {
            float h = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            if (!onGround)
            
                z = 0;
            rb.velocity = new Vector3(h * currentSpeed, rb.velocity.y, z * currentSpeed);

            if (h > 0 && !facingRight)
            {
               flip();
            }
            else if(h<0 && facingRight)
            {
                    flip();
            }

           /* if (jump)
            {
                    jump = false;
                    rb.AddForce(Vector3.up * jumpforce);
            }*/          
        }
    }
    void flip()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
