using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mov1 : MonoBehaviour

{
    //jumping v
    public float jumpH;
    public float jumpforce;
    private Vector3 jump;
    private Rigidbody rigg;

    //facing 
    private bool facingRight = true;

    //Animations
    private Animator Animator;
    private float runH;
    private float runV;
 
    void Start()
    {
        jump = new Vector3(0f, jumpH, 0f);
        rigg = GetComponent<Rigidbody>();
        Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Input.GetAxis("Horizontal") * Time.deltaTime * 5.0f, 0,0);
        transform.Translate(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * 5.0f);
        //Para WASD
        /*Animator.SetBool("Running", Input.GetKeyDown(KeyCode.D)
            || Input.GetKeyDown(KeyCode.A) 
            || Input.GetKeyDown(KeyCode.W)
            || Input.GetKeyDown(KeyCode.S));
        */
        runH = Input.GetAxisRaw("Horizontal");
        runV = Input.GetAxisRaw("Vertical");

        //Debug.Log(runH + runV);
        Animator.SetBool("Running", runH != 0.0f || runV != 0.0f);


        if (rigg.velocity.y == 0)
        {

            if (Input.GetKeyDown(KeyCode.Space))
            {
                rigg.AddForce(jump * jumpforce, ForceMode.Impulse);
            }
        }
    }
    private void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal"); //mov horizontal
        float z = Input.GetAxis("Vertical"); // Mov vertical
        
       
        if (h > 0 && !facingRight)
        {
            flip();
        }
        else if (h < 0 && facingRight)
        {
            flip();
        }
    }
    void flip()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
